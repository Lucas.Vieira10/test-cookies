import logo from './logo.svg';
import { useState } from 'react';
import './App.css';

function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [cookies, setCookies] = useState({});

  const login = async () => {
    setIsLoading(true);
    fetch('http://localhost:3333/login', {
      credentials: 'include',
    })
      .then(res => res.json())
      .catch(err => console.log({ err }))
      .then(data => {
        console.log({ data });
      })
      .finally(setIsLoading(false));
  };

  const verify = async () => {
    setIsLoading(true);
    fetch('http://localhost:3333/verify', {
      credentials: 'include',
    })
      .then(res => res.json())
      .catch(err => console.log({ err }))
      .then(data => {
        console.log({ data });
        setCookies(data);
      })
      .finally(setIsLoading(false));
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {isLoading && <p>Loading...</p>}

        <p>cookies:</p>
        <ul>
          {Object.entries(cookies).map(([key, value]) => (
            <li key={key}>
              {key}: {value}
            </li>
          ))}
        </ul>

        <div style={{ display: 'flex', gap: '10px' }}>
          <button onClick={login}>Login</button>
          <button onClick={verify}>Verify</button>
        </div>
      </header>
    </div>
  );
}

export default App;
