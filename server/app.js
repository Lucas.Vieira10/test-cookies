const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const app = express();
const port = 3333;

app.use(cors({ credentials: true, origin: 'http://localhost:3000' }));
app.use(cookieParser());

app.get('/login', (req, res) => {
  // res.cookie('test', 'value', { maxAge: 900000, httpOnly: true });
  res.setHeader('Set-Cookie', 'username=John Doe');
  res.json({ ok: true });
});

app.get('/verify', (req, res) => {
  console.log('All Cookies:', req.cookies);
  res.json(req.cookies);
});

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
